<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware' => ['web']], function () {
    Route::group(["as" => "admin.", "prefix" => "admin"], function () {
        Route::group(["middleware" => "auth:dcms"], function () {
            //PLANTGUIDE
            Route::group(["prefix" => "plantguide", "as" => "plantguide."], function () {
                Route::get("api/table", ["as" => "api.table", "uses" => "PlantController@getDatatable"]);
                Route::any("api/articles/table/{plant_id?}", array("as"=>"api.articles.table", "uses" => "PlantController@getArticlesDatatable"));
            });
            Route::resource("plantguide", "PlantController");

            //PLANTS
            Route::group(["prefix" => "plants", "as" => "plants."], function () {
                Route::group(["prefix" => "properties", "as" => "properties."], function () {
                    Route::any("api/table", ["as" => "api.table", "uses" => "PlantpropertyController@getDatatable"]);
                });
                Route::resource("properties", "PlantpropertyController");
            });
        });
    });
});
