<?php
namespace Dcms\Plants\Models;

use App;
use DB;
use Dcms\Core\Models\EloquentDefaults;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Plantcategorydetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_categories_language";

    public function plantcategory()
    {
        return $this->belongsTo('Dcms\Conditions\Models\Plantcategory', 'category_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo('Dcms\Core\Models\Languages\Language', 'language_id', 'id');
    }
}
