<?php
namespace Dcms\Plants\Models;

use App;
use DB;
use Dcms\Core\Models\EloquentDefaults;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Plant extends Model
{
    use NodeTrait;

    protected $connection = 'project';
    protected $table  = "plants";

    public function getLftName()
    {
        return 'lft';
    }

    public function getRgtName()
    {
        return 'rgt';
    }

    public function plantdetail()
    {
        return $this->hasMany('Dcms\Plants\Models\Plantdetail', 'plant_id', 'id');
    }

    public function productinformation()
    {
        // BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
        return $this->belongsToMany('Dcms\Products\Models\Information', 'products_information_group_to_plants', 'plant_id', 'information_id');
    }

    public function plantproperty()
    {
        /*
        The first argument in belongsToMany() is the name of the class Productdata, the second argument is the name of the pivot table, followed by the name of the product_id column, and at last the name of the product_data_id column.
        */
        return $this->belongsToMany('Dcms\Plants\Models\Plantproperty', 'plants_to_property', 'plant_id', 'plant_property_id');
    }

    public function conditions()
    {
        /*
        The first argument in belongsToMany() is the name of the class Productdata, the second argument is the name of the pivot table, followed by the name of the product_id column, and at last the name of the product_data_id column.
        */
        return $this->belongsToMany('Dcms\Conditions\Models\Conditions', 'conditions_to_plants', 'plants_id', 'conditions_id');
    }

    public function articles()
    {
        return $this->belongsToMany('Dcms\Dcmsarticles\Models\Article', 'article_to_plant', 'plant_id', 'article_id')->withTimestamps();
    }

    public function saveDepth()
    {
        $this->depth = self::withDepth()->find($this->id)->depth;
        $this->save();
    }
}
