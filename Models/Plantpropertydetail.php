<?php
namespace Dcms\Plants\Models;

use App;
use DB;
use Dcms\Core\Models\EloquentDefaults;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Plantpropertydetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_property_language";

    public function Plantproperty()
    {
        return $this->belongsTo('Dcms\Conditions\Models\Plantproperty', 'property_id', 'id');
    }
}
