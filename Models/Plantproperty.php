<?php
namespace Dcms\Plants\Models;

use App;
use DB;
use Dcms\Core\Models\EloquentDefaults;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Plantproperty extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_property";

    public function plantpropertydetail()
    {
        return $this->hasMany('Dcms\Conditions\Models\Plantpropertydetail', 'property_id', 'id');
    }
}
