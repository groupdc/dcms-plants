<?php
namespace Dcms\Plants\Models;

use App;
use DB;
use Dcms\Core\Models\EloquentDefaults;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Plantcategory extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_categories";

    public function plantcategorydetail()
    {
        return $this->belongsTo('Dcms\Conditions\Models\Plantcategorydetail', 'id');
    }

    public function plants()
    {
        return $this->hasMany('Dcms\Conditions\Models\Plant', 'category_id', 'id');
    }
}
