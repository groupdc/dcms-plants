@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>Plants</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li><a href="{!! URL::to('admin/plantguide') !!}"><i class="far fa-leaf"></i> Plants</a></li>
            @if(isset($Plant))
			    <li class="active"><i class="far fa-pencil"></i>  Plant {{$Plant->id}}</li>
            @else
			    <li class="active"><i class="far fa-plus-circle"></i> Create plant</li>
            @endif
        </ol>
    </div>

    <div class="main-content">

         @if(isset($Plant))
            {!! Form::model($Plant, array('route' => array('admin.plantguide.update', $Plant->id), 'method' => 'PUT', 'onsubmit'=>'return resetTables();')) !!}
        @else
            {!! Form::open(array('url' => 'admin/plantguide', 'onsubmit'=>'return resetTables();')) !!}
        @endif


        <div class="row">
            <div class="col-md-12">

                <div class="main-content-tab tab-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#information" role="tab" data-toggle="tab">Information</a></li>
                        <li><a href="#products" role="tab" data-toggle="tab">Products</a></li>
                        <li class=""><a href="#properties" role="tab" data-toggle="tab">Properties</a></li>
                        <li><a href="#conditions" role="tab" data-toggle="tab">Conditions</a></li>
                        <li><a href="#articles" role="tab" data-toggle="tab">Articles</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="information" class="tab-pane active">

                            @if($errors->any())
                                <div class="alert alert-danger">{!! HTML::ul($errors->all()) !!}</div>
                            @endif

                            <div class="form-group">
                                {!! Form::label('family', 'Family name') !!}
                                {!! Form::text('family', (old('family') ? old('family') : (isset($Plant->family)?$Plant->family:'') ), array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('botanic', 'Botanic name') !!}
                                {!! Form::text('botanic', (old('botanic') ? old('botanic') : (isset($Plant->botanic)?$Plant->botanic:'') ), array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('parent_id', 'Parent plant') !!}
                                {!! $parentplants !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('image', 'Image') !!}
                                <div class="input-group">
                                    {!! Form::text('image', old('image'), array('class' => 'form-control')) !!}
                                    <span class="input-group-btn">
                                      {!! Form::button('Browse Server', array('class' => 'btn btn-primary browse-server' , 'id'=>'browse_image')) !!}
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::checkbox('online', '1', null, array('class' => 'form-checkbox','id'=>'online'))  !!}
                                {!! HTML::decode(Form::label('online', 'Online', array('class' => (isset($Plant) && $Plant->online==1)?'checkbox active':'checkbox'))) !!}
                            </div>

                            @if(isset($languages))
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach($languages as $key => $language)
                                        <li class="
                                        {!! (( intval(session('overrule_default_by_language_id')) == intval($language->language_id)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '') ) !!}
                                        "><a href="{!! '#' . $language->language . '-' . $language->country !!}" role="tab"
                                                                                           data-toggle="tab"><img
                                                        src="{!! asset('/packages/Dcms/Core/images/flag-' . strtolower($language->country) . '.png') !!}" width="18"
                                                        height="12"/> {!! $language->language_name !!}</a></li>
                                    @endforeach
                                </ul>

                                <div class="tab-content">
                                    @foreach($languages as $key => $information)        
                                        <div id="{!! $information->language . '-' . $information->country !!}" class="tab-pane {!! (( intval(session('overrule_default_by_language_id')) == intval($information->language_id)) ? 'active' : ( ( intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '')) !!}">

                                            {!!Form::hidden('plant_detail_id[' . $information->language_id . ']', $information->plant_detail_id) !!} 

                                            <div class="form-group">
                                                {!! Form::label('common[' . $information->language_id . ']', 'Common') !!} 
                                                {!! Form::text('common[' . $information->language_id . ']', (old('common[' . $information->language_id . ']') ? old('common[' . $information->language_id . ']') : $information->common ), array('class' => 'form-control')) !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('description[' . $information->language_id . ']', 'Description') !!}
                                                {!! Form::textarea('description[' . $information->language_id . ']', (old('description[' . $information->language_id . ']') ? old('description[' . $information->language_id . ']') : $information->description ), array('class' => 'form-control ckeditor')) !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('information[' . $information->language_id . ']', 'Information') !!}
                                                {!! Form::textarea('information[' . $information->language_id . ']', (old('information[' . $information->language_id . ']') ? old('information[' . $information->language_id . ']') : $information->information ), array('class' => 'form-control ckeditor')) !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('title_tag[' . $information->language_id . ']', 'Title tag') !!} 
                                                {!! Form::text('title_tag[' . $information->language_id . ']', (old('title_tag[' . $information->language_id . ']') ? old('title_tag[' . $information->language_id . ']') : $information->title_tag ), array('class' => 'form-control')) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('meta_description[' . $information->language_id . ']', 'Meta Description') !!}
                                                {!! Form::text('meta_description[' . $information->language_id . ']', (old('meta_description[' . $information->language_id . ']') ? old('meta_description[' . $information->language_id . ']') : $information->meta_description ), array('class' => 'form-control')) !!}
                                            </div>

                                        </div>

                                    @endforeach
                                </div>
                            @endif

                        </div>

                        <div id="products" class="tab-pane">
                            <!-- Labels -->
                            <div class="form-group">

                                <table id="datatable" class="table table-hover table-condensed" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Title</th>
                                        <th>Country</th>
                                        <th>Division</th>
                                        <th>ID</th>         
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <div id="properties" class="tab-pane">

                            <?php
                            // find magicsuggest like it is done on "VraagHetAan"
                            //ref nicolasbize.github.io/magicsuggest/
                            $dropdown = array();

                            foreach ($oProperties as $Property) {
                                if (!isset($dropdown[$Property->property_name]['options'])) {
                                    $dropdown[$Property->property_name]['options'] = array();
                                }
                                if (!is_null($Property->property_language)) {
                                    $dropdown[$Property->property_name]['options'][$Property->id] = $Property->property_language;
                                }

                                if (!isset($dropdown[$Property->property_name]['selected'])) {
                                    $dropdown[$Property->property_name]['selected'] = array();
                                }
                                if ($Property->selected == 1) {
                                    $dropdown[$Property->property_name]['selected'][$Property->id] = $Property->id;
                                }
                            }
                            ?>
                            @foreach($dropdown as $name => $ddsettings)
                                <div class="form-group">
                                    {!! Form::label(str_replace(' ', '', $name), $name) !!}
                                    {!! Form::select('multiproperty_id['.$name.'][]', $dropdown[$name]['options'],  $dropdown[$name]['selected'], array('class' => 'form-control ddProperty','multiple')) !!}
                                </div>
                            @endforeach
                        </div>

                        <div id="conditions" class="tab-pane">
                            <!-- Labels -->
                            <div class="form-group">

                                <table id="datatableconditions" class="table table-hover table-condensed" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Title</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <div id="articles" class="tab-pane">
                            <div class="form-group">
                                <table id="articles-datatable" class="table table-hover table-condensed" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Articles</th>
                                        <th>Country</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">

                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>

                </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

@stop

@section("script")

    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/jquery-ui-autocomplete.min.js') !!}"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('/packages/Dcms/Core/css/jquery-ui-autocomplete.css') !!}">

    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/adapters/jquery.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckfinder.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckbrowser.js') !!}"></script>

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script type="text/javascript">
        conditionTable = null;
        productsTable = null;
        articlesTable = null;
        function resetTables(){
            conditionTable.search('').draw();
            productsTable.search('').draw();
            articlesTable.search('').draw();
            return true;
        }

        $(document).ready(function () {

            // CKFinder for CKEditor
            CKFinder.setupCKEditor(null, '/ckfinder/');

            // Browser --) $(this).attr("id")
            $(".browse-server").click(function () {
                var returnid = $(this).attr("id").replace("browse_", "");
                BrowseServer('Images:/', returnid);
            })

            // CKEditor
            $("textarea[id^='description']").ckeditor();
            $("textarea[id^='body']").ckeditor();
            $("textarea[id^='information']").ckeditor();

            // Bootstrap Tabs
            $(".tab-container .nav-tabs a").click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // //Bootstrap Datepicker
            // $(".date").datetimepicker({
            //     todayHighlight: true,
            //     autoclose: true,
            //     pickerPosition: "bottom-left"
            // });
            // Articles datatable
            @php
             $id_helper = (isset($Plant)?$Plant->id:0);
            @endphp

            productsTable = $('#datatable').DataTable({
                        "paging": false,
                        "processing": true,                        
                        "ajax": "{{ route('admin.products.information.api.plantrelationtable', [ $id_helper ]) }}",
                        "columns": [
                            {data: 'radio', name: 'radio'},
                            {data: 'title', name: 'title'},
                            {data: 'language', name: 'language'},
                            {data: 'catalogue', name: 'catalogue'},
                            {data: 'information_group_id', name: 'information_group_id'},
                        ],
                        "createdRow": function( row, data, cells ) {
                            if (~data['radio'].indexOf('checked')) {
                                
                            $(row).addClass('selected');
                            }
                        }
                    });

            $('#datatable').on('click', 'tbody tr', function (e) {
                if($(e.target).is('input[type=checkbox]')) {
                $(this).toggleClass('selected');
                } else {
                    var clicked = $(this).find('input:checkbox');
                    if ( clicked.is(':checked')) {
                        clicked.prop('checked', false);
                    } else {
                        clicked.prop('checked', true);
                    }
        
                    $(this).toggleClass('selected');
                }
            });

            conditionTable = $('#datatableconditions').DataTable({
                        "paging": false,
                        "processing": true,
                        "ajax": "{{ route('admin.conditions.api.plantrelationtable', [ $id_helper ]) }}",
                        "columns": [
                            {data: 'radio', name: 'radio', orderable: false, searchable: false, width: '10%'},
                            {data: 'condition', name: 'condition'}
                        ]
                    });

            articlesTable = $('#articles-datatable').DataTable({
                "paging": false,
                "processing": true,
                "ajax": "{{ route('admin.plantguide.api.articles.table', [$id_helper]) }}",
                "columns": [
                    {data: 'radio', name: 'radio', orderable: false, searchable: false, width: '10%'},
                    {data: 'title', name: 'title'},
                    {data: 'language', name: 'language'}
                ]
            });

        });
    </script>

@stop
