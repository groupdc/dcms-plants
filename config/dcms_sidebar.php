<?php

return [
    "Plants" => [
        "icon"  => "fa-leaf",
        "links" => [
            ["route" => "admin/plantguide", "label" => "Plants", "permission" => "plants"],
            ["route" => "admin/plants/properties", "label" => "Properties", "permission" => "plants"],
        ],
    ],
];
